import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

/**
 * This is Delete Dialog Component that takes many props named 
 * data
 * text
 * opendialog
 * setdialog 
 * Request
 * delete function
 */

const Delete = ({ text, opendialog, setdialog, data, Request }) => {
  const DeleteCustomer = async (id) => {
    try {
      const res = await fetch(
        `${process.env.REACT_APP_API_URL}/customer/delete/${id}`,
        {
          method: "Delete",
        }
      );
      const response = await res.json();
      console.log(response);
      if (response.success === true) {
        setdialog(!opendialog.isVisible);
        Request();
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Dialog
      open={opendialog}
      onClose={() => setdialog(!opendialog)}
      keepMounted
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{text}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description"></DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button 
        variant="contained" 
        onClick={() => setdialog(!opendialog)}>
          no
        </Button>
        <Button
          variant="contained"
          color="error"
          sx={{ color: "white" }}
          onClick={(e) => DeleteCustomer(data)}
          autoFocus
        >
          delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default Delete;
