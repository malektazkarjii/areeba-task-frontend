import React, { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { TextField } from "@mui/material";
import axios from "axios";
/**
 * This is Edit Dialog Component that takes many props named text opendialog setdialog data Request and it has the edit function
 */

const EditDialog = ({ text, opendialog, setdialog, data, Request }) => {
  const { customer_name, customer_address, customer_number, _id } = data;
  const [name, setname] = useState(customer_name);
  const [address, setaddress] = useState(customer_address);
  const [number, setnumber] = useState(customer_number);
  const [valid,setvalid]=useState("");

  const Update = async (id) => {
    try {
      const res = await axios.put(
        `${process.env.REACT_APP_API_URL}/customer/update/${id}`,
        {
          customer_name: name,
          customer_address: address,
          customer_number: number,
        }
      );
      const response = await res.data;
      console.log(response);
      if (response.success === true) {
        setdialog(!opendialog.isVisible);
        Request();
      }else if(response.status === 203 || response.status === 400){
        setvalid(response.message);
        console.log(response.message)
        console.log(valid)
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Dialog
      open={opendialog}
      onClose={() => setdialog(!opendialog)}
      keepMounted
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{valid}</DialogTitle>
      <DialogContent>
        {/* <DialogContentText id="alert-dialog-description"> */}
        <TextField
          style={{marginTop:'20px'}}
          onChange={(e) => {
              setname(e.target.value)
          }}
          label="name"
          defaultValue={customer_name}
        />
        <TextField
          style={{marginTop:'20px'}}
          onChange={(e) => 
            {setnumber(e.target.value);
            setvalid("")}
        }
          label="Number"
          defaultValue={customer_number}
          error={valid !== ""}
            helperText={valid}
          />
        <TextField
         style={{marginTop:'20px'}}
          onChange={(e) => setaddress(e.target.value)}
          label="Address"
          defaultValue={customer_address}
        />

        {/* </DialogContentText> */}
      </DialogContent>
      <DialogActions>
        <Button variant="contained" onClick={() => setdialog(!opendialog)}>
          No
        </Button>
        <Button
          variant="contained"
          color="error"
          sx={{ color: "white" }}
          onClick={(e) => Update(_id)}
          autoFocus
        >
          Update
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EditDialog;
