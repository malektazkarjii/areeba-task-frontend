import React, { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { TextField } from "@mui/material";
import axios from "axios";

/**
 * This is the Add Dialog Component that takes many  props named 
 * text:To display the text inside ,
 * opendialog : to open the dialog when the state is true,
 * setdialog : to set setstate from false to true when closing the dialog,
 * Request : to fetch the data after adding the object,
 * also and it has the add function,
 */

const AddDialog = ({ text, opendialog, setdialog, Request }) => {
  const [valid,setvalid]=useState("");
  const [name, setname] = useState("");
  const [address, setaddress] = useState("");
  const [number, setnumber] = useState("");

  const Create = async () => {
    try {
      const res = await axios.post(
        `${process.env.REACT_APP_API_URL}/customer/create`,
        {
          customer_name: name,
          customer_address: address,
          customer_number: number,
        }
      );
      const response = await res.data;
      console.log(response);
      if (response.success === true) {
        setdialog(!opendialog.isVisible);
        Request();
      }else if(response.status === 203 || response.status === 400){
        setvalid(response.message);
        console.log(response.message)
        console.log(valid)
      }
      setdialog(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Dialog
      open={opendialog}
      onClose={() => setdialog(!opendialog)}
      keepMounted
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{text}</DialogTitle>
      <DialogContent>
        <TextField onChange={(e) => setname(e.target.value)} label="name" />
        <TextField
         onChange={(e) => {setnumber(e.target.value);
        setvalid("")}}
          label="Number"
          error={valid !== ""}
          helperText={valid}
          />
        <TextField
          onChange={(e) => setaddress(e.target.value)}
          label="Address"
        />
      </DialogContent>
      <DialogActions>
        <Button variant="contained" onClick={() => setdialog(!opendialog)}>
          No
        </Button>
        <Button
          variant="contained"
          color="error"
          sx={{ color: "white" }}
          onClick={Create}
          autoFocus
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddDialog;
