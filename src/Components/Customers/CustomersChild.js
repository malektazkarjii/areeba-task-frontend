import React from "react";

/**
 * This is Customer Child Component that takes only one prop named data
 */

const CustomerChild = ({data}) => {
 
    const {customer_name, customer_address, customer_number} = data;
    return (
        <div style={{width:"40vw",height:"20vh",margin:"10vh 4.5vw"}}>
            <h1>{customer_name}</h1>
            <h1>{customer_number}</h1>
            <h1>{customer_address}</h1>
        </div>
    )
}

export default CustomerChild;