import './App.css';
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import Customers from "./pages/Customers"
import NotFound from './pages/NotFound';


function App() {
  return (
    <div className="App">
       <Router>
          <Routes>
            <Route index path="/" element={<Customers />} />
            <Route path='*' element={<NotFound/>} />
          </Routes>
      </Router>
    </div>
  );
}

export default App;
