import React, { useEffect, useState } from "react";
import CustomerChild from "../Components/Customers/CustomersChild";
import Delete from "../Components/deleteDialog";
import EditDialog from "../Components/EditDialog";
import AddDialog from "../Components/AddDialog";
import { Button } from "@mui/material";
/**
 * This is Customer Parent Component that have the fetching data function
 */

const Customers = () => {
    const [customers,setcustomers]=useState([]);
    const[openAdd,setAdd]=useState(false);
    const[openEdit,setEdit]=useState({
        isVisible: false,
        data : null,
    })
    const[openDialog,setopenDialog]=useState({
        isVisible : false,
        data: null
       })

    const Request =async () => {
        try{
        const res = await fetch(`${process.env.REACT_APP_API_URL}/customer`)
        const response = await res.json();
        // console.log(response);
        setcustomers(response)
        }catch(error){
            console.log(error);
        }
    }




useEffect(()=>{
    Request();
},[])

    return (
        <>
        <Button
        style={{position:"fixed",marginTop:"50px"}}
        onClick={()=>window.scrollTo(0,0)}
        color="inherit"
         variant="contained"
        >Scroll To Top</Button>
        <Button
        color="inherit"
        variant="contained"
         onClick={()=>window.scrollTo(0,document.body.scrollHeight)}
         style={{position:"fixed",marginTop:"130px"}}
        >Scroll To Buttom</Button>
        <Button
         color="inherit"
         variant="contained"
         sx={{margin:"50px 50px 0 75vw"}}
         onClick={()=>setAdd(!openAdd)}>Create New Customer</Button>

        <div style={{display:"flex",flexWrap:"wrap",margin:"30px auto"}}>
            {openAdd &&
            <AddDialog 
                     text="Create new customer"
                     opendialog={openAdd}
                     setdialog={setAdd}
                     Request={Request}
            />
            }
            {openDialog.isVisible &&
            <Delete     
                     Request={Request}
                     text="Are you sure About deleting this Customers"
                     opendialog={openDialog.isVisible}
                     setdialog={setopenDialog}
                     data={openDialog.data}
            />  
            }
            {openEdit.isVisible &&
            <EditDialog 
                     Request={Request}
                     text="Are you sure About updating this Customers"
                     opendialog={openEdit.isVisible}
                     setdialog={setEdit}
                     data={openEdit.data}
            />
            }
            
            {customers?.data?.map((customer,index)=>{
                return (
                    <div key={customer._id}>
                    <CustomerChild data={customer}
                    />
                  <Button 
                  color="error"
                  variant="contained"
                  onClick={()=>setopenDialog({
                      isVisible:!openDialog.isVisible,
                      data:customer._id})}>DELETE
                  </Button>
                  <Button 
                  sx={{marginLeft:"20px"}}
                  color="success"
                  variant="contained"
                  onClick={()=>setEdit({
                      isVisible : !openEdit.isVisible,
                      data : customer})}>EDIT
                  </Button>
                    </div>
                )
            })}
        </div>
</>
    )
}

export default Customers;