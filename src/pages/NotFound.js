import React from "react";

/**
 * This is NotFound route page component
 */

const NotFound = () => {
    return (
        <div>
            <h1>Page Not Found In this website</h1>
            </div>
    )
}

export default NotFound;