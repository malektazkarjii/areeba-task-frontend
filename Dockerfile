FROM node:15.5.0

WORKDIR /app/areeba-task-frontend

COPY ./package.json .
COPY ./public ./public
COPY ./src ./src
RUN npm install -- force

COPY . .

EXPOSE 3000

CMD ["npm","start"]